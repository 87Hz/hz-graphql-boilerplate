import * as restify from 'restify'
import { graphqlRestify, graphiqlRestify } from 'apollo-server-restify'
import { makeExecutableSchema } from 'graphql-tools'

const server = restify.createServer({
  name: 'myGraphqlServer',
  version: '1.0.0'
})

const books = [
  {
    title: "Harry Potter and the Sorcerer's stone",
    author: 'J.K. Rowling',
  },
  {
    title: 'Jurassic Park',
    author: 'Michael Crichton',
  },
]

const typeDefs = `
  type Query { books: [Book] }
  type Book { title: String, author: String }
`

const resolvers = {
  Query: { books: () => books }
}

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
})

const graphQLOptions = { schema }

server.use(restify.plugins.bodyParser())
server.use(restify.plugins.queryParser())

server.post('/graphql', graphqlRestify(graphQLOptions))
server.get('/graphiql', graphiqlRestify({ endpointURL: '/graphql' }))

server.listen(8080, () => {
  console.log(`${server.name} listening @ ${server.url}`)
})